# JWT

A very simple JWT library that exposes three functions:

- Encode
- Decode
- Verify

## Encoding a JWT with a payload

```javascript
const payload = {
    username: "chromagna",
    iat: Date.now(),
    exp: Date.now() + 3600,
};

// Encode accepts a third argument - the signature method.
// The default is HS256.
const tokenString = Encode(payload, "private-key");
```

## Decoding a JWT into its parts

```javascript
const { header, payload, signature } = Decode(tokenString);
```

## Verify a JWT signature

The third parameter to `Verify` expects an object with keys that specify which claims to validate.

By default, the `sig` claim is always verified. To verify other claims, you will need to explicitly pass
them as keys to this parameter. These claims must also exist within the original payload that was encoded in the JWT.

The result of each claim validation will be a boolean.

```javascript
const result = Verify(tokenString, "private-key", { exp: true });

// The signature claim is always included in the validation result.
if (result.sig) {
    // Token signature verified
}

// The expiration claim is validated because we explicitly chose to do so.
if (result.exp) {
    // Token has expired
}
```
