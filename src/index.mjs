import * as crypto from "crypto";

const ALGORITHMS = ["HS256", "HS384", "HS512", "RS256", "RS384", "RS512"];

const isValidAlgorithm = (alg) => ALGORITHMS.includes(alg);

const CreateHmacAlg = (bits) => {
    const sign = function sign(encoded, secret) {
        const sig = crypto.createHmac("sha" + bits, secret).update(encoded).digest("base64");
        return sig;
    };
    const verify = function verify(encoded, signature, secret) {
        const sig = sign(encoded, secret);
        return sig === signature;
    };

    return { sign, verify };
};

const CreateRsaAlg = (bits) => {
    const sign = function sign(encoded, secret) {
        const sig = crypto.createSign("SHA" + bits).update(encoded).sign(secret.toString(), "base64");
        return sig;
    };
    const verify = function verify(encoded, signature, secret) {
        const verifier = crypto.createVerify("RSA-SHA" + bits);
        verifier.update(encoded);
        return verifier.verify(secret, signature, "base64");
    };
    return { sign, verify };
};

const Algorithms = {
    HS256: CreateHmacAlg(256),
    HS384: CreateHmacAlg(384),
    HS512: CreateHmacAlg(256),
    RS256: CreateRsaAlg(256),
    RS384: CreateRsaAlg(384),
    RS512: CreateRsaAlg(512),
};

const JsonBase64Encode = (obj) => {
    const js = JSON.stringify(obj);
    return Base64ToUrlEncoded(Buffer.from(js).toString("base64"));
};

const JsonBase64Decode = (str) => {
    const decoded = Buffer.from(UrlEncodedToBase64(str), "base64").toString("utf-8");
    return JSON.parse(decoded);
};

const Base64ToUrlEncoded = (base64) => base64.replace(/=/g, "").replace(/\+/g, "-").replace(/\//g, "_");

const UrlEncodedToBase64 = (base64url) => {
    base64url = base64url.toString();

    let padding = 4 - base64url.length % 4;

    if (padding !== 4) {
        for (let i = 0; i < padding; i++) base64url += "=";
    }

    return base64url.replace(/\-/g, "+").replace(/_/g, "/");
};

const DEFAULT_VERIFY_OPTS = {};


/**
 * Encode a new JWT for client distribution.
 *
 * Usage:
 * let payload = {
 *      some: "data",
 *      iat: Date.now(),
 *      exp: Date.now() + 60 * 1000,
 * };
 * const tokenString = Encode(payload, "my-secret-key");
 */
export const Encode = (payload, key, alg = "HS256") => {
    if (!isValidAlgorithm(alg)) throw new Error(`Invalid algorithm. Got "${alg}". Must be one of ${ALGORITHMS}.`);

    const header_b64 = JsonBase64Encode({alg, type: "JWT"});
    const payload_b64 = JsonBase64Encode(payload);
    const unsigned = `${header_b64}.${payload_b64}`;
    const signer = Algorithms[alg];
    const sig = Base64ToUrlEncoded(signer.sign(unsigned, key));

    return `${unsigned}.${sig}`;
};

/**
 * Decide a JWT into its parts.
 *
 * Usage:
 * let { header, payload, signature } = Decode(tokenString);
 */
export const Decode = (token) => {
    const parts = token.split(".");

    if (parts.length !== 3) throw new Error(`Invalid token. Must have 3 parts. Got ${parts.length} parts.`);

    const header = JsonBase64Decode(parts[0]);
    const payload = JsonBase64Decode(parts[1]);
    const signature = Buffer.from(UrlEncodedToBase64(parts[2]), "base64");

    return {header, payload, signature};
};

export const Verify = (token, key, opts = DEFAULT_VERIFY_OPTS) => {
    const decoded = Decode(token);
    const payload = decoded.payload;
    const parts = token.split(".");
    const alg = opts.alg || decoded.header.alg;
    const now = Date.now();
    const verifier = Algorithms[alg];
    const result = {};

    /**
     * Usage:
     * if (!result.sig) { ... token signature cannot be verified ... }
     */
    if (opts.sig === undefined || opts.sig === true) {
        result.sig = verifier.verify(`${parts[0]}.${parts[1]}`, UrlEncodedToBase64(parts[2]), key);
    }

    /**
     * REQUIRED. Expiration time on or after which the ID Token MUST NOT be accepted for processing.
     *
     * Usage:
     * if (result.exp) { ... expired token ... }
     */
    if (opts.exp === true && payload.exp !== undefined) {
        result.exp = payload.exp < now;
    }

    /**
     * OPTIONAL. Time at which the JWT was issued.
     */
    if (opts.iat !== undefined) {
        result.iat = payload.iat === opts.iat;
    }

    /**
     * Issuer Identifier for the Issuer of the response. The iss value is a case sensitive URL using the https scheme that contains scheme, host, and optionally, port number and path components and no query or fragment components.
     *
     * e.g. https://self-issued.me
     */
    if (opts.iss !== undefined) {
        result.iss = payload.iss === opts.iss;
    }

    /**
     * OPTIONAL. JWT ID. A unique identifier for the token, which can be used to prevent reuse of the token.
     */
    if (opts.jti !== undefined) {
        result.jti = payload.jti !== opts.jti;
    }

    /**
     * OPTIONAL. Subject Identifier. A locally unique and never reassigned identifier within the Issuer for the End-User, which is intended to be consumed by the Client, e.g., 24400320 or AItOawmwtWwcT0k51BayewNvutrJUqsvl6qs7A4.
     */
    if (opts.sub !== undefined) {
        result.sub = payload.sub === opts.sub;
    }

    /**
     * Audience(s) that this ID Token is intended for.
     *
     * e.g. https://client.example.com
     */
    if (opts.aud !== undefined) {
        result.aud = payload.aud === opts.aud;
    }

    return result;
}
